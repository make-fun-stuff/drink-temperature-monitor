# Adafruit MLX90614 Library
The sketch depends on Adafruit's helpful library to handle reading
the temperature data from the sensor. You should follow Adafruit's
instructions to install the library:

[Click here](https://learn.adafruit.com/using-melexis-mlx90614-non-contact-sensors/wiring-and-test#download-adafruit-mlx90614-581957-7)

# YouTube Video
[Click here](https://youtu.be/4gJ1Rk6mqHM)