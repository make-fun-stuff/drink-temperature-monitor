#include <Adafruit_MLX90614.h>

// Feel free to add your favorite temperature scale if it isn't here!
enum TemperatureScale {
  FAHRENHEIT,
  CELSIUS,
  KELVIN,
  RANKINE,
  LEIDEN,
  NEWTON,
  REAUMUR,
  ROMER,
  DELISLE
};

/** 
 * Choose your favorite temperature scale
 */
const TemperatureScale tempScale = KELVIN;

/**
 * The lower and upper temperature bounds that the monitor will
 * care about. Any temperature at or below the lower bound will
 * result in only one light being on, anything above the upper
 * bound will light up all five lights. The range of temperatures
 * will be divided into three equal sized sub-ranges, and the
 * middle lights will be lit up based on those ranges.
 * 
 * These values will be interpreted as whatever temperature
 * scale you've set as the value of the "tempScale" constant.
 */
const int minTemp = 305;
const int maxTemp = 320;

const float bucketSize = (maxTemp - minTemp) / 3;
const int ledPins[5] = {4, 6, 8, 10, 12};

Adafruit_MLX90614 sensor = Adafruit_MLX90614();

void setup() {
  Serial.begin(9600);
  Serial.println("Adafruit MLX90614 test");

  // setup temperature sensor
  sensor.begin();

  // setup pins for lights
  for(int ndx = 0; ndx < 5; ndx++) {
    pinMode(ledPins[ndx], OUTPUT);
    digitalWrite(ledPins[ndx], HIGH);
  }
}

void controlLights(int numLights) {
  for (int ndx = 0; ndx < 5; ndx++) {
    digitalWrite(ledPins[ndx], ndx < numLights ? LOW : HIGH);
  }
}

float readTemperature() {
  if (tempScale == FAHRENHEIT) {
    return sensor.readObjectTempF();
  }
  if (tempScale == CELSIUS) {
    return sensor.readObjectTempC();
  }
  if (tempScale == KELVIN) {
    return sensor.readObjectTempC() + 273.15;
  }
  if (tempScale == RANKINE) {
    return sensor.readObjectTempF() + 459.67;
  }
  if (tempScale == LEIDEN) {
    return sensor.readObjectTempC() + 253;
  }
  if (tempScale == NEWTON) {
    return sensor.readObjectTempC() * 33 / 100;
  }
  if (tempScale == REAUMUR) {
    return sensor.readObjectTempC() * 4 / 5;
  }
  if (tempScale == ROMER) {
    return sensor.readObjectTempC() * 21 / 40 + 7.5;
  }
  if (tempScale == DELISLE) {
    return (100 - sensor.readObjectTempC()) * 3 / 2;
  }
  // you've done something wrong
  return 0;
}

int getNumberOfLights(float temp) {
  if (temp <= minTemp) {
    return 1;
  }
  if (temp <= minTemp + bucketSize) {
    return 2;
  }
  if (temp <= minTemp + 2 * bucketSize) {
    return 3;
  }
  if (temp <= maxTemp - bucketSize) {
    return 4;
  }
  return 5;
}

void loop() {
  const float temp = readTemperature();
  const int numLights = getNumberOfLights(temp);
  controlLights(numLights);

  Serial.print("Temperature = ");
  Serial.print(temp);
  Serial.print(" (");
  Serial.print(numLights);
  Serial.println(" lights)");
}
